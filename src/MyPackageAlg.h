#ifndef MYPACKAGE_MYPACKAGEALG_H
#define MYPACKAGE_MYPACKAGEALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

#include "AsgTools/AnaToolHandle.h"
//#include "MCTruthClassifier/MCTruthClassifier.h"
#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "AthAnalysisBaseComps/AthAnalysisHelper.h"

#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"

//Example ROOT Includes
//#include "TTree.h"
//#include "TH1D.h"


//class MCTruthClassifier;
class MyPackageAlg: public ::AthAnalysisAlgorithm { 
 public: 
  MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MyPackageAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


  // set up variables for filling the TTree

    int m_eventNumber; 

    double m_electronTruePt; // (trueParticlePtr)->pt();
    double m_electronTrueEta;  // (trueParticlePtr)->eta();
    double m_electronTrueE;  // (trueParticlePtr)->e();

    double m_electronEta; // (photon)->eta();
    double m_electronPt; // (photon)->pt();
    double m_electronE; // (photon)->e();

    double m_electronCaloE; // (photon)->caloCluster()->e();
    double m_electronCaloEta;  // (photon)->caloCluster()->eta();

    double m_electronE_calibrated; // (photon)->e() calibrated;

    // std::pair<unsigned int, unsigned int> particleTypeAndOrigin = m_truthClassifier->particleTruthClassifier(photon) ;
    int m_particleType; // particleTypeAndOrigin.first
    int m_particleOrigin; // particleTypeAndOrigin.second

    int m_truthParticle_Type;  // associatedTruthParticleTypeAndOrigin.first;
    int m_truthParticle_Origin;  // associatedTruthParticleTypeAndOrigin.second;

    int m_conversionType; // photon->conversionType()
    bool m_isTrueConvertedPhoton; // xAOD::EgammaHelpers::isTrueConvertedPhoton( photon )

    int m_particleTruthType; // xAOD::TruthHelpers::getParticleTruthType(*photon)







 private: 

   //Example algorithm property, see constructor for declaration:
   //int m_nProperty = 0;

   //Example histogram, see initialize method for registration to output histSvc
   //TH1D* m_myHist = 0;
   TTree* m_myTree = 0;

  //ToolHandle<IMCTruthClassifier>        m_truthClassifier;
  //asg::AnaToolHandle<MCTruthClassifier> m_truthClassifier;
  asg::AnaToolHandle<IMCTruthClassifier> m_truthClassifier;
  asg::AnaToolHandle<CP::EgammaCalibrationAndSmearingTool> energy_tool;


}; 

#endif //> !MYPACKAGE_MYPACKAGEALG_H

// MyPackage includes
#include "MyPackageAlg.h"

#include "xAODEventInfo/EventInfo.h"

//This should be the container for electrons
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/ElectronContainer.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"

// this should help me to find the 'truth electron' to a given reconstructed electron
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODEgamma/EgammaxAODHelpers.h"

//#include "MCTruthClassifier/IMCTruthClassifier.h"
//#include "MCTruthClassifier/MCTruthClassifier.h"

//to do shallow copies of xAODs
#include <xAODCore/ShallowCopy.h>

MyPackageAlg::MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator ) : 
AthAnalysisAlgorithm( name, pSvcLocator )
//,m_truthClassifier("MCTruthClassifier/MCTruthClassifier")
{

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  //declareProperty("MCTruthClassifier",   m_truthClassifier);

}


MyPackageAlg::~MyPackageAlg() {}


StatusCode MyPackageAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream

// //====================================================================================
// // setting up out TTree
// //====================================================================================

  m_myTree = new TTree("tree","tree");
  CHECK( histSvc()->regTree("/MYSTREAM/tree", m_myTree) ); //registers tree to output stream inside a sub-directory

  m_myTree->Branch("EventNumber", &m_eventNumber);

  m_myTree->Branch("eTruePt", &m_electronTruePt);     // (trueParticlePtr)->pt();
  m_myTree->Branch("eTrueEta", &m_electronTrueEta);   // (trueParticlePtr)->eta();
  m_myTree->Branch("eTrueE", &m_electronTrueE);

  m_myTree->Branch("eEta", &m_electronEta); 
  m_myTree->Branch("ePt", &m_electronPt);
  m_myTree->Branch("eE", &m_electronE);
  m_myTree->Branch("eE_calibrated", &m_electronE_calibrated);

  m_myTree->Branch("eCaloE", &m_electronCaloE);
  m_myTree->Branch("eCaloEta", &m_electronCaloEta);;

  m_myTree->Branch("truthTypeOfParticle", &m_particleTruthType);

  m_myTree->Branch("conversionType", &m_conversionType);
  m_myTree->Branch("isTrueConvertedPhoton", &m_isTrueConvertedPhoton);

  m_myTree->Branch("particleType", &m_particleType);
  m_myTree->Branch("ParticleOrigin", &m_particleOrigin);

  m_myTree->Branch("truthParticle_Type", &m_truthParticle_Type);
  m_myTree->Branch("truthParticle_Origin", &m_truthParticle_Origin);


  //ATH_CHECK(m_truthClassifier.retrieve());


// //====================================================================================
// // MCTruthClassifier tool
// //====================================================================================


  //m_truthClassifier.setTypeAndName("MCTruthClassifier/MCTruthClassifier");
  //CHECK( m_truthClassifier.initialize() );
  m_truthClassifier.setTypeAndName("MCTruthClassifier/MyMCTruthClassifier");
  CHECK( m_truthClassifier.initialize() );


// //====================================================================================
// // Egamma calibration tool
// //====================================================================================

 energy_tool.setTypeAndName("CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool");

 energy_tool.setProperty("ESModel", "es2017_R21_v1"); // if running on Release 21
 //energy_tool.setProperty("ESModel", "es2017_R21_PRE"); // if running on Release 21
 //energy_tool.setProperty("ESModel", "es2016data_mc15c_final"); // if running on Release 20



 energy_tool.setProperty("randomRunNumber", "123456");

 // Turn off gain correction
 energy_tool.setProperty("useGainCorrection", 0);

 //-----------------------------------------------------------------------------------------
 // !! Remove scales and smearing for computation nTuples
 //-----------------------------------------------------------------------------------------
 energy_tool.setProperty("doSmearing", 0);
 energy_tool.setProperty("doScaleCorrection", 0);


 CHECK( energy_tool.initialize() );




  return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  using namespace MCTruthPartClassifier;


  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //


  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  CHECK( evtStore()->retrieve( eventInfo , "EventInfo" ) );
  ATH_MSG_DEBUG("eventNumber=" << eventInfo->eventNumber() );
  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram
    static const MCTruthPartClassifier::ParticleDef partDef;  // get the dicts for the MCTruthPartClassifier

//  // Retrieve truthEvents
//  const xAOD::TruthEventContainer* truthEvents = 0;
//  CHECK( evtStore()->retrieve( truthEvents, "TruthEvents" ) );


 /*
  // Retrieve truthParticles 
  const xAOD::TruthParticleContainer* truthParticles = 0;
  CHECK( evtStore()->retrieve(truthParticles,"TruthParticles") );

  //Make a shallow copy of the electron container, so that we can apply the energy corrections (the original photons seem to be static / write protected)
  std::pair<xAOD::TruthParticleContainer*, xAOD::ShallowAuxContainer*> truth_shallowCopy  = xAOD::shallowCopyContainer(*truthParticles); 

  //Record in StoreGate  -- not reallly sure what this does, but taken from https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection/trunk/src/testAthenaEgammaCalibTool.cxx
  CHECK( evtStore()->record( truth_shallowCopy.first,  "TruthCollectionCorr") );
  CHECK( evtStore()->record( truth_shallowCopy.second, "TruthCollectionCorrAux.") );
  
  //Iterate over the shallow copy
  xAOD::TruthParticleContainer* copiedTruth = truth_shallowCopy.first;
  xAOD::TruthParticleContainer::iterator truth_iterator      = copiedTruth->begin();
  xAOD::TruthParticleContainer::iterator truth_iterator_last = copiedTruth->end();
  unsigned int iTrue = 0;


  for (; truth_iterator != truth_iterator_last; ++truth_iterator, ++iTrue) {
      xAOD::TruthParticle* trueParticle = *truth_iterator; // get the current photon
    auto res = m_truthClassifier->particleTruthClassifier( trueParticle );
    ATH_MSG_DEBUG("Truth particle #" <<iTrue << "  Type = " << partDef.sParticleType[res.first] << " Origin = " << partDef.sParticleOrigin[res.second]);
  }
  */


  //----------------------------
  // Photons
  //--------------------------- 

  // get photon container of interest
  const xAOD::PhotonContainer* photons = 0;
  CHECK( evtStore()->retrieve( photons, "Photons" ) );

  //Make a shallow copy of the electron container, so that we can apply the energy corrections (the original photons seem to be static / write protected)
  std::pair<xAOD::PhotonContainer*, xAOD::ShallowAuxContainer*> photons_shallowCopy  = xAOD::shallowCopyContainer(*photons); 

  //Record in StoreGate  -- not reallly sure what this does, but taken from https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection/trunk/src/testAthenaEgammaCalibTool.cxx
  CHECK( evtStore()->record( photons_shallowCopy.first, "ElectronCollectionCorr") );
  CHECK( evtStore()->record( photons_shallowCopy.second, "ElectronCollectionCorrAux.") );

  //Iterate over the shallow copy
  xAOD::PhotonContainer* copiedPhotons = photons_shallowCopy.first;
  xAOD::PhotonContainer::iterator photon_iterator      = copiedPhotons->begin();
  xAOD::PhotonContainer::iterator photon_iterator_last = copiedPhotons->end();
  unsigned int i = 0;

  // Loop over all photons in this container
  for (; photon_iterator != photon_iterator_last; ++photon_iterator, ++i) {

    xAOD::Photon* photon = *photon_iterator; // get the current photon

    // get the pointer to the truth particle associated with the current photon
    const xAOD::TruthParticle* trueParticlePtr = xAOD::TruthHelpers::getTruthParticle(*photon); // of the particleTruthType is != 2 we seem to get the null pointer more often than not.



    if(trueParticlePtr){ // we need the true pt, eta, and energy for out analysis. So if there's no associated true photon, just skip it
      
      m_eventNumber = eventInfo->eventNumber();

      m_electronTrueEta = trueParticlePtr->eta();
      m_electronTruePt =  trueParticlePtr->pt();
      m_electronTrueE =   trueParticlePtr->e();

      m_electronEta     = photon->eta();
      m_electronPt      = photon->pt();
      m_electronE       = photon->e();
      m_electronCaloE   = photon->caloCluster()->e();
      m_electronCaloEta = photon->caloCluster()->eta();

      energy_tool->applyCorrection(*photon);
      m_electronE_calibrated = photon->e();

      m_conversionType = photon->conversionType() ;

      m_isTrueConvertedPhoton = xAOD::EgammaHelpers::isTrueConvertedPhoton( photon );

      m_particleTruthType = xAOD::TruthHelpers::getParticleTruthType(*photon);

      std::pair<unsigned int, unsigned int> particleTypeAndOrigin = m_truthClassifier->particleTruthClassifier(photon);
      ATH_MSG_DEBUG("photon #" <<i << "  Type = " << partDef.sParticleType[particleTypeAndOrigin.first] << " Origin = " << partDef.sParticleOrigin[particleTypeAndOrigin.second]);

      m_particleType   = particleTypeAndOrigin.first;
      m_particleOrigin = particleTypeAndOrigin.second;

      std::pair<unsigned int, unsigned int> associatedTruthParticleTypeAndOrigin = m_truthClassifier->particleTruthClassifier(trueParticlePtr);
      ATH_MSG_DEBUG("TruePh #" <<i << "  Type = " << partDef.sParticleType[associatedTruthParticleTypeAndOrigin.first] << " Origin = " << partDef.sParticleOrigin[associatedTruthParticleTypeAndOrigin.second]);
  
      m_truthParticle_Type   = associatedTruthParticleTypeAndOrigin.first;
      m_truthParticle_Origin = associatedTruthParticleTypeAndOrigin.second;
      // truthParticle_Type and truthParticleOrigin are all 14 and 3, does not look helpfull

      m_myTree->Fill(); // put data into our tree / nTuple


      ATH_MSG_DEBUG( "  photon in container number " << i);

      ATH_MSG_DEBUG( "  photon true eta = " << m_electronTrueEta );
      ATH_MSG_DEBUG( "  photon true pt = "  << m_electronTruePt );
      ATH_MSG_DEBUG( "  photon true e = "   << m_electronTrueE );


      ATH_MSG_DEBUG( "  photon eta = " << m_electronEta );
      ATH_MSG_DEBUG( "  photon pt = "  << m_electronPt );
      ATH_MSG_DEBUG( "  photon e = "   << m_electronE );
      ATH_MSG_DEBUG( "  photon calo e = "    << m_electronCaloE );
      ATH_MSG_DEBUG( "  photon calo eta = "  << m_electronCaloEta );
      ATH_MSG_DEBUG( "  photon (corrected) e = " << m_electronE_calibrated );


      ATH_MSG_DEBUG( "  photon conversionType = "    << m_conversionType );
      ATH_MSG_DEBUG( "isTrueConvertedPhoton = " << m_isTrueConvertedPhoton );
      //ATH_MSG_INFO( "isTrueConvertedPhoton = " << m_isTrueConvertedPhoton );

      ATH_MSG_DEBUG( "  photon particleTruthType = " << m_particleTruthType );


      ATH_MSG_DEBUG( "  photon particleType = "   << m_particleType );
      ATH_MSG_DEBUG( "  photon particleOrigin = " << m_particleOrigin );
    } // if(trueParticlePtr)




  } // end for loop over photons


  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}



#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "POOLAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]  #register output files like this. MYSTREAM is used in the code

athAlgSeq += CfgMgr.MyPackageAlg()                               #adds an instance of your alg to the main alg sequence
#svcMgr.MessageSvc.setDebug += ["MyPackageAlg"] # uncomment this if you want Debug level messages from the 'MyPackageAlg'

#---- Options you could specify on command line -----
jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=100
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
jps.AthenaCommonFlags.FilesInput = ["/afs/cern.ch/user/c/chweber/myWorkspace/data/mc16_13TeV.423001.ParticleGun_single_photon_egammaET.merge.AOD.e3566_s3113_r9388_r9315/AOD.11223032._004166.pool.root.1"]        #set on command-line with: --filesInput=...

ToolSvc += CfgMgr.MCTruthClassifier("MyMCTruthClassifier")
include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above


Initial setup:
	mkdir MyProject
	cd MyProject
	mkdir source build run
	cd build
	acmSetup AthAnalysis,21.2,latest



When returning:
	cd MyProject/build
	acmSetup

For Compiling:
	acm compile


For running locally:
	cd $TestArea/../run
	athena MyPackage/MyPackageAlgJobOptions.py --evtMax=10


For running on the grid
	cd $TestArea/../run
	lsetup panda
	pathena MyPackage/MyPackageAlgJobOptions.py --inDS=mc16_13TeV.423001.ParticleGun_single_photon_egammaET.merge.AOD.e3566_s3113_r9388_r9315/ --outDS=user.$USER.mc16.423001.ParticleGun_single_photon_egammaET.deriv.AOD.EnergyResolution.nTuple.es2016data_mc15c_final.201811062117

